﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public CharacterController charactorController;

    public float horizontalInput;
    public float verticalInput;


    public float speed;

    float timeElapsed;
    float lerpDuration = 3;

    float ver;
    Vector3 var;
    void Start()
    {
        charactorController.detectCollisions = false;
    }//start

    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        Vector3 charactorDirection = new Vector3(horizontalInput , 0f , verticalInput);

        charactorController.Move(charactorDirection * speed * Time.deltaTime);

        if (verticalInput > 0)
        {
            transform.rotation = Quaternion.Euler(0f, 0, 0f);
        }
        if (verticalInput < 0)
        {
            transform.rotation = Quaternion.Euler(0f, 180, 0f);
        }
        if (horizontalInput > 0)
        {
            transform.rotation = Quaternion.Euler(0f, 90, 0f);
        }
        if (horizontalInput < 0)
        {
            transform.rotation = Quaternion.Euler(0f, -90, 0f);
        }
    }//update

}//class