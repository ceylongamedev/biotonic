﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CamController : MonoBehaviour
{
    public Transform player;
    public Vector3 offSet;
    public Camera cam;

    public float mouseWheelSpeed;
    float mouseWheelInput;

    private Vector3 velocity = Vector3.zero;
    private float smoothTime = 5;

    void Start()
    {
        offSet = transform.position - player.position;
    }


    void LateUpdate()
    {

            Vector3 targetPos = player.position + offSet;
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smoothTime);


        transform.LookAt(player, Vector3.up) ;
    }

    void Update()
    {
  
        mouseWheelInput -= Input.GetAxis("Mouse ScrollWheel") * mouseWheelSpeed;
        mouseWheelInput = Mathf.Clamp(mouseWheelInput, 45, 65);

        cam.fieldOfView = mouseWheelInput;

    }
}//class
